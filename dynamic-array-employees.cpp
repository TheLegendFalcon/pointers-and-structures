#include <iostream>
#include <new>
#include <string>

struct Employees {

  std::string fName, lName, email, phone_num;
  double id;
  double salary;
  Employees* manager;
};

void print_employee(Employees& e) {
  std::cout << "\n\tName: "
	    << e.fName
	    << " "
	    << e.lName

	    << "\n\tID: "
	    << e.id

	    << "\n\tE-Mail: "
	    << e.email

	    << "\n\tPhone Number: "
	    << e.phone_num

	    << "\n\tSalary: "
	    << e.salary;

  if (e.manager != nullptr) {
    std::cout << "\n\tManager's Name: "
	      << e.manager->fName
	      << " "
	      << e.manager->lName;
  }
  std::cout << std::endl;
}

  std::string prompt_string(const char* question) {
  std::string value;
  std::cout << question;

  std::getline(std::cin >> std::ws, value);
  return value;
}

double prompt_double(const char* question) {
  double value;
  std::cout << question;
  std::cin >> value;
  return value;
}

int main() {
  int num_of_employees;
  Employees* employees;

  num_of_employees =
    prompt_double("Please enter the amount of Employees you will need: ");

  employees = new Employees[num_of_employees];

  for (int i = 0; i < num_of_employees; i++) {
    employees[i].id = i;
    employees[i].manager = nullptr;

    employees[i].fName = prompt_string("\nEnter in the employees first name: ");
    employees[i].lName = prompt_string("\nEnter in the employees last name: ");
    employees[i].email = prompt_string("\nEnter the employees email: ");
    employees[i].phone_num = prompt_string("\nEnter in the employees phone number: ");
    employees[i].salary = prompt_double("\nEnter the employees monthly salary: ");
  }

  std::cout << "\nYour employees are listed: " << std::endl;

  for (int i = 0; i < num_of_employees; i++) {
    print_employee(employees[i]);
  }

  delete[]employees;
}
