#include <iostream>

int main() {

  //standard double variable
  double my_num = 2.4;


  //pointer to a double
  double* ptr_to_my_num = &my_num;

  std::cout << "Variable: " << my_num
	    << " address: " << &my_num << "\n";

  std::cout << "Pointer: " << ptr_to_my_num
	    << " address: " << &ptr_to_my_num
	    << " dereference: " << *ptr_to_my_num << "\n";
}
