#include <iostream>

//Reverse Character Decleration

void print_arr(char array[], int length)
{
  std::cout << "[";

  for (int i = 0; i < length - 1; i++) {
    std::cout << array[i] << " ";
  }
    std::cout << array[length - 1] << "]" << std::endl;
}

void reverse(char s[], int length)
{
  int startlength = 0;
  int endlength = length - 1;

  while (startlength < endlength) {
    char temp = s[startlength];

    s[startlength] = s[endlength];
    s[endlength] = temp;

    startlength++;
    endlength --;
  }
}

//Main program function

int main(){
  char rca[] = "Harry";

  std::cout << "Actual input of array: " << std::endl;
  print_arr(rca, (sizeof(rca) / sizeof(*rca)));

  reverse(rca, (sizeof(rca) / sizeof(*rca)));
  std::cout << "Reversed output of array: " << std::endl;
  print_arr(rca, (sizeof(rca) / sizeof(*rca)));
}
