#include <iostream>
#include <string>

struct Employees {

  std::string fName, lName, email, phone_num;
  double id;
  double salary;
  Employees* manager;
};

void print_employees(Employees& e){
  std::cout << "\n\tName: "
	    << e.fName
	    << " "
	    << e.lName
    
	    << "\n\tID: "
	    << e.id
    
	    << "\n\tE-Mail: "
    
	    << e.email
	    << "\n\tPhone Number"
    
	    << "\n\tSalary: "
	    << e.salary;

  if (e.manager != nullptr) {
    std::cout << "\n\tManager Name: "
	      << e.manager->fName
	      << " "
	      << e.manager->lName;
  }

  std::cout <<  std::endl;
}

int main(){

  Employees manager = {
    "Joseph",
    "Joestar",
    "joseph.joestar@jojo.com",
    "+971 56 666 6420",
    1,
    500000,
    nullptr,
  };

  Employees staff1 = {

    "Jotaro",
    "Kujo",
    "kujo.jotaro@jojo.com",
    "+971 50 452 6240",
    2,
    25000,
    &manager,
  };

  Employees staff2 = {

    "Gyro",
    "Zeppeli",
    "gyro.zeppeli@jojo.com",
    "+971 56 890 6969",
    3,
    20000,
    &manager,
  };

  std::cout << "Employees that have been created: ";

  print_employees(manager);
  print_employees(staff1);
  print_employees(staff2);
}
